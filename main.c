// Código do projeto do Werner, primeiro
// faremos ele sem usar os registradores
// do AVR, para garantir a lógica pronta.
// após isso faremos com uso dos registradores.

#define luzFreio 10
#define setaEsquerda 9
#define setaDireita 8
#define botaoDireito 12
#define botaoEsquerdo 13
#define ladoDireito 3
#define ladoEsquerdo 2
#define coracao 6
#define acelerometro A1
#define aproximacao A0
#define alertaCoracao 5
#define alertaAproximacao 4

//teste

unsigned long tempo;
int batimentos;
bool ultimoValorCoracao;

unsigned long tempoSetaEsquerda;
unsigned long tempoSetaDireita;

void setup() {
  setPins();

  attachInterrupt(digitalPinToInterrupt(2), ligaSetaEsquerda, RISING);
  attachInterrupt(digitalPinToInterrupt(3), ligaSetaDireita, RISING);
}

void loop() {
  tempo = millis();

  batimentos = 0;

  while(millis() - tempo <= 60000) {
    if(digitalRead(coracao) && ultimoValorCoracao == 0){
      batimentos++;
    }

    freio(); // criar função para ativar luz de freio

    // colocar os dois caras de baixo em uma função
    if(digitalRead(botaoDireito)) {
      while(!digitalRead(ladoDireito)) {
        // faz algo setaDireita
      }
    }

    if(digitalRead(botaoEsquerdo)) {
      while(!digitalRead(ladoEsquerdo)){
        // faz algo setaEsquerda
      }
    }
    // os dois de cima no caso

    alertAproximacao(); // criar função para avisar da aproximação traseira




  }

  alertaBatimentos(); // criar função para avisar ou não do excesso de batimentos cardiacos

}

void setPins() {
  pinMode(luzFreio, OUTPUT);
  pinMode(setaDireita, OUTPUT);
  pinMode(setaEsquerda, OUTPUT);
  pinMode(ladoEsquerdo, INPUT);
  pinMode(ladoDireito, attachInterrupthsrUT);
  pinMode(botaoEsquerdo, INPUT);
  pinMode(botaoDireito, INPUT);
  pinMode(coracao, INPUT);
  pinMode(acelerometro, INPUT);
  pinMode(aproximacao, INPUT);
  pinMode(alertaAproximacao, OUTPUT);
  pinMode(alertaCoracao, OUTPUT);
}

void ligaSetaEsquerda() {
}

void alertAproximacao() {

}

void alertaBatimentos() {

}

void freio(){

}
